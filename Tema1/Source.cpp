#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <memory>
#include "Utils.h"
#include <conio.h>

auto main() -> int
{
	{
		Utils<int>test;
		std::vector<int> values;
		values.push_back(1);
		values.push_back(2);
		values.push_back(7);
		values.push_back(6);
		values.push_back(5);
		test.setVector(values);
		std::cout << std::endl << "Mean is: " << test.mean();

		/*	Utils<int> lv_nUtils;
			Utils<double> lv_dfUtils;


			auto const lv_nMaxElement = lv_dfUtils.mf_MaxElement(7, 10);
			auto const lv_dfMaxElement = lv_dfUtils.mf_MaxElement(2.3, 4.5);

			std::cout << lv_nMaxElement << std::endl;
			std::cout << lv_dfMaxElement << std::endl << std::endl;
		}

		{
			std::map<int, std::string> lv_Map;
			lv_Map.insert(std::make_pair(5, std::string("FirstInesrtion")));
			lv_Map.insert(std::make_pair(3, std::string("SecondInsertion")));

			for (auto const mapIterator : lv_Map)
			{
				std::cout << mapIterator.first << " " << mapIterator.second << std::endl;
			}

		}

		{
			std::map<Utils<int>, int, Utils<int>::Comparator> lv_UtilsMaps;
			Utils<int> lv_nUtils1;
			Utils<int> lv_nUtils2;
			lv_UtilsMaps.insert(std::make_pair(lv_nUtils1, 0));
			lv_UtilsMaps.insert(std::make_pair(lv_nUtils2, 1));

		}

		std::shared_ptr<Utils<int>> lv_sharedPtr1 = std::make_shared<Utils<int>>();
		{
			std::shared_ptr<Utils<int>> lv_sharedPtr2 = lv_sharedPtr1;
			std::cout << std::endl << "Refference count: " << lv_sharedPtr1.use_count() << std::endl;
		}
		std::cout << "Refference count: " << lv_sharedPtr1.use_count() << std::endl << std::endl;


		*/

		_getch();
		return EXIT_SUCCESS;




	}
}